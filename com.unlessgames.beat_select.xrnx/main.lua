local name = "beat_select"

local dialog = nil

local prefs = renoise.Document.create("ScriptingToolPreferences") {
}
renoise.tool().preferences = prefs

function clamp(a, min, max)
  return math.min(math.max(min, a), max)
end

function sign(x)
  if(x < 0) then return -1 else return 1 end
end


function wrap(v, x, l)
  local r = ((v + x - 1) % l) + 1
  if(x == -1 and v == 1) then
    r = l
  end
  return r
end

function navigate_beats(offset)
  local song = renoise.song()
  local length = song.selected_pattern.number_of_lines
  local line = song.selected_line_index
  local lpb = song.transport.lpb

  line = line + lpb * offset
  if(line > length) then
    line = clamp(line - length, 1, length)
  elseif(line < 1) then
    line = clamp(line + length, 1, length)
  end

  song.selected_line_index = line
end

function cursor_to_selection(song)
  local note_column = song.selected_note_column_index
  local effect_column = song.selected_effect_column_index
  local column = note_column

  if(note_column == 0) then
    column = song.selected_track.visible_note_columns + effect_column
  end

  local end_column = song.selected_track.visible_note_columns + song.selected_track.visible_effect_columns

  return {
    start_track = song.selected_track_index,
    end_track = song.selected_track_index,
    start_column = 1,
    end_column = end_column,
    start_line = song.selected_line_index,
    end_line = song.selected_line_index,
  }
end

function incomplete_selection(song, selection)
  local start_not_on_beat = (selection.start_line - 1) % song.transport.lpb ~= 0
  local end_not_on_beat = (selection.end_line - 1) % song.transport.lpb ~= (song.transport.lpb - 1)
  return start_not_on_beat or end_not_on_beat
end


function inside_selection(song, s)
  local l = song.selected_line_index
  local t = song.selected_track_index
  return l >= s.start_line and l <= s.end_line and t >= s.start_track and t <= s.end_track
end

function select_beats(song, selection, a, b)
  local lpb = song.transport.lpb
  local length = math.floor(song.selected_pattern.number_of_lines / lpb)

  a = clamp(a, 0, length - 1)
  b = clamp(b, 0, length - 1)

  local start_beat = math.min(a, b)
  local end_beat = math.max(a, b)

  local beats = math.abs(start_beat - end_beat) + 1
  if beats > 1 then
    local message = beats .. " beats selected"
    message = message .. " [ ".. start_beat + 1 .. " - " .. end_beat + 1 .. " ]"
    renoise.app():show_status(message)
  end
  selection.start_line = start_beat * lpb + 1
  selection.end_line = end_beat * lpb + lpb
  return selection
end


function beat_selecting(song)
  local start_on_beat = (song.selected_line_index - 1) % song.transport.lpb == 0
  local end_on_beat = (song.selected_line_index - 1) % song.transport.lpb == (song.transport.lpb - 1)
  return start_on_beat or end_on_beat
end

function start_beat_select(direction)
  local s = renoise.song()
  local selection = s.selection_in_pattern

  if selection == nil or not inside_selection(s, selection) or not beat_selecting(s)then
    selection = cursor_to_selection(s)
  end

  if incomplete_selection(s, selection) then
    local b = math.floor((s.selected_line_index - 1) / s.transport.lpb)
    selection = select_beats(s, selection, b, b)
    if direction == 1 then
      s.selected_line_index = selection.end_line
    else
      s.selected_line_index = selection.start_line
    end
    return {
      selection = selection,
      was_incomplete = true
    }
  else
    return{
      selection = selection,
      was_incomplete = false
    }
  end
end

function extend_selection_by_beats(direction)
  local s = renoise.song()
  local init = start_beat_select(direction)
  local selection = init.selection

  if init.was_incomplete then
    -- return
  else
    local length = s.selected_pattern.number_of_lines
    local line = s.selected_line_index
    local lpb = s.transport.lpb

    local start_beat = math.floor((selection.start_line - 1) / lpb)
    local end_beat = math.floor((selection.end_line - 1) / lpb)
    local upwards_select = (s.selected_line_index - 1) % s.transport.lpb == 0
    local beats = math.abs(start_beat - end_beat)
    if beats == 0 then
      if direction == -1 then
        if upwards_select then
          selection = select_beats(s, selection, start_beat - 1, end_beat)
        end
        s.selected_line_index = selection.start_line
      else
        if not upwards_select then
          selection = select_beats(s, selection, start_beat, end_beat + 1)
        end
        s.selected_line_index = selection.end_line
      end
    else
      if upwards_select then
        selection = select_beats(s, selection, start_beat + direction, end_beat)
        s.selected_line_index = selection.start_line
      else
        selection = select_beats(s, selection, start_beat, end_beat + direction)
        s.selected_line_index = selection.end_line
      end
    end
  end

  s.selection_in_pattern = selection
end

function set_selection_tracks(song, selection, start_track, end_track)
  selection.start_track = math.min(start_track, end_track)
  selection.end_track = math.max(start_track, end_track)
  selection.end_column = song.tracks[selection.end_track].visible_note_columns + song.tracks[selection.end_track].visible_effect_columns
  return selection
end

function extend_selection_by_tracks(direction)
  local s = renoise.song()
  local start = start_beat_select(direction)
  local selection = start.selection
  if start.was_incomplete then
  else
    local start_track = selection.start_track
    local end_track = selection.end_track
    local length = math.abs(start_track - end_track)

    if direction == -1 then
      if start_track == s.selected_track_index then
        start_track = clamp(start_track - 1, 1, #s.tracks)
        s.selected_track_index = start_track
      else
        end_track = clamp(end_track - 1, start_track, #s.tracks)
        s.selected_track_index = end_track
      end
    else
      if start_track == s.selected_track_index then
        start_track = clamp(start_track + 1, 1, #s.tracks)
        s.selected_track_index = start_track
      else
        end_track = clamp(end_track + 1, start_track, #s.tracks)
        s.selected_track_index = end_track
      end
    end
    selection = set_selection_tracks(s, selection, start_track, end_track)
  end
  s.selection_in_pattern = selection
end

renoise.tool():add_keybinding {
  name = "Pattern Editor:Selection:Select until previous beat",
  invoke = function(repeated)
    extend_selection_by_beats(-1)
  end
}

renoise.tool():add_keybinding {
  name = "Pattern Editor:Selection:Select until next beat",
  invoke = function(repeated)
    extend_selection_by_beats(1)
  end
}

renoise.tool():add_keybinding {
  name = "Pattern Editor:Selection:Select until previous track",
  invoke = function(repeated)
    extend_selection_by_tracks(-1)
  end
}

renoise.tool():add_keybinding {
  name = "Pattern Editor:Selection:Select until next track",
  invoke = function(repeated)
    extend_selection_by_tracks(1)
  end
}

renoise.tool():add_keybinding {
  name = "Pattern Editor:Navigation:Jump one beat down",
  invoke = function(repeated)
    navigate_beats(1)
  end
}
renoise.tool():add_keybinding {
  name = "Pattern Editor:Navigation:Jump one beat up",
  invoke = function(repeated)
    navigate_beats(-1)
  end
}
renoise.tool():add_keybinding {
  name = "Pattern Editor:Selection:Deselect",
  invoke = function(repeated)
    renoise.song().selection_in_pattern = nil
  end
}
renoise.tool():add_keybinding {
  name = "Phrase Editor:Selection:Deselect",
  invoke = function(repeated)
    if renoise.song().selected_phrase ~= nil then
      renoise.song().selection_in_phrase = nil
    end
  end
}

_AUTO_RELOAD_DEBUG = function()
end

print(name .. " loaded.")