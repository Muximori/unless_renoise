require "data"

name = "line_grab"

dialog = nil

prefs = renoise.Document.create("ScriptingToolPreferences") {

}
tool = renoise.tool()
tool.preferences = prefs

function clamp(a, min, max)
  return math.min(math.max(min, a), max)
end

function wrap(v, x, l)
  local r = ((v + x - 1) % l) + 1
  if(x == -1 and v == 1) then
    r = l
  end
  return r
end

function table:index_of(t, v)
  for i, k in pairs(t) do
    if(k == v) then
      return i
    end
  end
  return -1
end

function string:split(s, separator)
  local a = {}
  for str in string.gmatch(s, "([^" .. separator .. "]+)") do
    table.insert(a, str)
  end
  return a
end

function line_is_empty(l) 
  for _, c in pairs(l.note_columns) do
    if(not c.is_empty) then return false end
  end
  for _, c in pairs(l.effect_columns) do
    if(not c.is_empty) then return false end
  end
  return true
end

function find_nearest_empty_line(song, offset)
  local length = song.selected_pattern.number_of_lines

  local start_line = wrap(song.selected_line_index, offset, length)
  local i = start_line
  local search_steps = 0
  local lines = song.selected_pattern.tracks[song.selected_track_index].lines
  while search_steps < length do
    if line_is_empty(lines[i]) then
      return i
    end
    i = wrap(i, offset, length)
    search_steps = search_steps + 1
  end
  return start_line
end


function move_selected_line_vertically(song, offset)
  local track = song.selected_track_index

  local from = song.selected_line_index
  local target_index  = find_nearest_empty_line(song, offset)

  local current_data = clone_line(song.selected_pattern.tracks[track].lines[from])
  
  local current = song.selected_pattern.tracks[track].lines[from]

  local next = song.selected_pattern.tracks[track].lines[target_index]

  current:copy_from(next)

  load_line_data_to(current_data, next)

  song.selected_line_index = target_index
end

function closest_empty_track(song, line_index, offset)
  local track = song.selected_track_index
  local track_count = #(song.tracks)
  local target_track = wrap(track, offset, track_count)
  for i = 0, track_count - 1 do
    local next_line = song.selected_pattern.tracks[target_track].lines[line_index]
    if song.tracks[target_track].type == renoise.Track.TRACK_TYPE_SEQUENCER and line_is_empty(next_line) then
      return target_track
    end
    target_track = wrap(target_track, offset, track_count)
  end
  return 0
end

function move_selected_line_horizontally(song, offset)
  local track = song.selected_track_index
  local line_index = song.selected_line_index
  local start_line = song.selected_pattern.tracks[track].lines[line_index]

  local next_track = closest_empty_track(song, line_index, offset)
  if next_track ~= 0 then
    local next_line = song.selected_pattern.tracks[next_track].lines[line_index]    
    local current_data = clone_line(start_line)
    next_line:copy_from(start_line)
    start_line:clear()
    song.selected_track_index = next_track
  end
end

function clone_device(device, target_container, target_index)
  local song = renoise.song()
  target_index = clamp(target_index, 2, #target_container.devices + 1)
  local dest = target_container:insert_device_at(device.device_path, target_index)
  for i = 1, #device.parameters do
    local p = device.parameters[i]
    oprint(p)
    dest.parameters[i].value = p.value
  end
  dest.display_name = device.display_name
  return target_index
end

function move_track_device_horizontally(song, dir)
  local d = song.selected_track_device_index
  if d == 0 then 
    return
  else
    local t = song.selected_track_index
    local t2 = clamp(t + dir, 1, #song.tracks)
    if t ~= t2 then
      local i = clone_device(song.selected_track_device, song.tracks[t2], d)
      song.selected_track:delete_device_at(d)
      song.selected_track_index = t2
      song.selected_track_device_index = i
    end
  end
end

function move_chain_device_vertically(song, dir)
  local d = song.selected_sample_device_index
  if d == 0 then 
    return
  else
    local ci = song.selected_sample_device_chain_index
    local ci2 = clamp(ci + dir, 1, #song.selected_instrument.sample_device_chains)
    if ci ~= ci2 then
      local i = clone_device(song.selected_sample_device, song.selected_instrument.sample_device_chains[ci2], d)
      song.selected_sample_device_chain:delete_device_at(d)
      song.selected_sample_device_chain_index = ci2
      song.selected_sample_device_index = i
    end
  end
end

tool:add_keybinding {
  name = "Mixer:Edit:Move Device Right",
  invoke = function(repeated)
    move_track_device_horizontally(renoise.song(), 1)
  end
}

tool:add_keybinding {
  name = "Mixer:Edit:Move Device Left",
  invoke = function(repeated)
    move_track_device_horizontally(renoise.song(), -1)
  end
}
tool:add_keybinding {
  name = "Sample FX Mixer:Device:Move Device Up",
  invoke = function(repeated)
    move_chain_device_vertically(renoise.song(), -1)
  end
}

tool:add_keybinding {
  name = "Sample FX Mixer:Device:Move Device Down",
  invoke = function(repeated)
    move_chain_device_vertically(renoise.song(), 1)
  end
}

tool:add_keybinding {
  name = "Pattern Editor:Tools:Move line down",
  invoke = function(repeated)
    move_selected_line_vertically(renoise.song(), 1)
  end
}

tool:add_keybinding {
  name = "Pattern Editor:Tools:Move line up",
  invoke = function(repeated)
    move_selected_line_vertically(renoise.song(), -1)
  end
}

tool:add_keybinding {
  name = "Pattern Editor:Tools:Move line right",
  invoke = function(repeated)
    move_selected_line_horizontally(renoise.song(), 1)
  end
}

tool:add_keybinding {
  name = "Pattern Editor:Tools:Move line left",
  invoke = function(repeated)
    move_selected_line_horizontally(renoise.song(), -1)
  end
}

_AUTO_RELOAD_DEBUG = function()
end

print(name .. " loaded. ")