# tools for [renoise](https://www.renoise.com)

## installation

* [download](https://gitlab.com/unlessgames/unless_renoise/-/archive/master/unless_renoise-master.zip) this repo
* unzip it somewhere and drop one of the **.xrnx** folders onto a Renoise window
* assign shortcuts inside *Edit / Preferences / Keys* to your new commands

## support

Consider donating to help me create and update these tools, games and other things I make, any amount is highly appreciated!

[![](https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=8LE6YQGVJLREW)    

Also

- report bugs 
- improve the manual by telling me if something is unclear
- contribute code
- enjoy using my tools

## contributions

* [Michael Grogan](https://gitlab.com/Muximori) added fixes to command_palette for Mac

# tools

[command_palette](#command_palette) - for a keyboard-focused workflow  
[value_stepper](#value_stepper) - step values in the pattern  
[line_grab](#line_grab) - move lines around in the pattern  
[beat_select](#beat_select) - select lines by beats and tracks  
[tint](#tint) - highlight the selected track with its color
[zoner](#zoner) - distribute sample along velocity and more  


# command_palette

Navigate and configure a song using the keyboard.

Provides two key-bindings in *Global / Tools*

- `Open command palette`
- `Repeat last command`

### usage

- open the command palette
- search for what you want by typing letters
- change the selected command with up or down
- type some number
- press enter to apply and exit
- delete your input with backspace
- escape will cancel the command and close the popup
- press left to recall previous commands

### command types

- `number` commands are the most common, they will show a `#` symbol in your top bar when you select them. These accept a single number that you can type in as soon as you have the command selected and it will be executed immeditately when the input is changed.

- `action` commands either require no input or more complex input that can be set in a separate window. These won't do anything until you press enter. They have the sign `>`

- `string` commands can for example rename things. These are noted with the `=` sign and they will open a separate text input window for you to provide the text string


Some commands open secondary palettes, these work the same way: just type or navigate until you have the match then hit `enter`. Some number commands will let you excecute them without input and they will open a search palette for you to pick something by text instead of providing an index.

For example if you run the 't' or 'select track' command without an input number it will list all the tracks by name and will let you search and navigate it similarly to the main palette. The same thing works for instruments, samples or sections and even DSP devices across the whole song. If you name your things right this can help you a lot in navigating using the keyboard.


# value_stepper


![](https://forum.renoise.com/uploads/default/original/2X/c/c04753c1b55335beff4385310ef9e124181b9335.gif)  

Adjust values via keyboard by a set amount. Similar to the *Transpose One Note Up* inside Renoise but works for other values as well such as instrument number, volume, panning, delay and effect parameters.

Provides four key-bindings in *Pattern Editor / Column Operations*:

- Step value up
- Step value down
- Step value up (by 16)
- Step value down (by 16)

The default step size is 1 but you can change it in *Tools / value stepper*

Note: the greater stepping on notes is done by octaves instead of 16.

### settings

- **step size** (default is 1)
- **ignore edit mode** (enabling this will make stepping work even if you aren't in edit mode)
- **select instrument with step** (when changing instrument numbers also select the instrument)
- **auto-blank** (a panning value of `40` (centered) and a volume value of `80` (max volume) gets converted to an empty column (technically `FF`) to have a cleaner pattern)
- **relative mode** (see below)
- **repeat last** (starts relative stepping from last value as opposed to last + offset)

### block selection

When selecting multiple columns and lines, you need to place your cursor inside the selection on the column you want to step. You can only block-step columns of the same type at once. (this extends to the type of commands inside FX columns as well, for example when you place your cursor on a `-Sxx` command only other `-Sxx` commands will get stepped inside your selection)

Inside the settings you can find different modes that control how block stepping is done.

- **step non-empty values** (all selected columns that have other than the default value will get stepped, this mode pairs well with disabling the **auto-blank** setting)
- **step values with notes** (only steps empty columns if they have a note)
- **step all values** (steps everything inside the selection)

### relative mode

![](https://forum.renoise.com/uploads/default/original/2X/5/555c60c894e06ae0f5330e1ab0deb1ab5090f353.gif)

There is an optional **relative mode** that can be enabled in the settings window. It starts stepping empty columns from the nearest non-empty value inside the pattern (searching backwards). Good for writing melodies and creating continuous effect lines.

A keybinding exist for toggling relative mode in "Pattern Editor / Tools" called :
  - Toggle relative mode (value stepper)

# line_grab

![](https://forum.renoise.com/uploads/default/original/2X/f/f41623205081da888de9f5fa4a058963e799b2ae.gif)

Move whole lines vertically or horizontally into available empty lines.

Provides four key-bindings in *Pattern Editor / Tools*

- Move line up
- Move line right
- Move line down
- Move line left

Additionaly there are some more to move selected DSP devices in a similar manner inside the mixer and sample effects view. Renoise provides keybindings for moving a device inside its own track or chain but it won't let you move devices between them, this tool implements these missing actions.

Two in *Mixer / Edit* to move devices across tracks

- Move Device Right
- Move Device Left

and two in *Sample FX Mixer / Device* to move devices across sample effect chains

- Move Device Up
- Move Device Down


# beat_select

![](https://forum.renoise.com/uploads/default/original/2X/f/fd965f9bf09a0b1313783a26c2f215ba62c3c810.gif)

Faster selection inside the pattern editor by beats and tracks


Provides four key-bindings in *Pattern Editor / Selection*

- Select until previous beat
- Select until next beat
- Select until previous track
- Select until next track

And an additional two in *Pattern Editor / Navigation*

- Jump one beat down
- Jump one beat up


# tint

![](https://forum.renoise.com/uploads/default/original/3X/9/1/91aeeb7d8d9a6fcc5c4d5db6d55bf5573e4138b4.gif)

Highlight the track at the cursor with its color. 

Additional settings can be found in the *Tools/tint* menu

- Adjust the `opacity` of the highlight if it is too much or too little for you
- Batch recolor all (or a selection) of tracks with a rainbow spectrum
- Customize the spectrum to perfect your hue game

![](https://forum.renoise.com/uploads/default/original/3X/8/f/8fa587c33fe2cb34de137f4986104fcf95f5a461.gif)

# zoner

![](https://forum.renoise.com/uploads/default/original/3X/8/9/89d1a46049a654019e8d9c2f6b9e9ac7c5533e49.gif)

Implements a dialog to generate stacked keyzones for multiple samples distributed along the velocity range or octaves etc.

The dialog can be opened three ways 

- using the right-click menu inside the Keyzones editor view
- pressing the `Open zoner` keybinding (if it's configured)
- from *Tools / zoner*
