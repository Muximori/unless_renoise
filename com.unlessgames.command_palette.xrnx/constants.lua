CommandResult = {
  quit = -1,
  continue = 0,
  success = 1,
}

CommandType = {
  number = 1,
  string = 2,
  action = 3,
}

DeviceSubType = {
	volume = 1,
	panning = 2,
	pitch = 3
}

TextFormat = {
	lowercase = 1,
	capitalized = 2,
	uppercase = 3,
}

function default_prefs()
	return {
		wrapping = true,
		use_mono_font = false,
		spacing = 1,
		width = 320,
		max_results = 12,
		ninja_mode = false,
		schedule_loops_when_playing = false,
		parameter_step_division = 256,
		text_format = TextFormat.lowercase,
		show_tips = false,
		binds = "e:e",
  }
end