
function clamp(a, min, max)
  return math.min(math.max(min, a), max)
end

function clampwrap(a, min, max, allow_wrap)
  if allow_wrap then
    if a < min then return max - a
    elseif a > max then return min
    else
      return a
    end
  else
    return clamp(a, min, max)
  end
end

function sign(x)
  if(x < 0) then return -1 else return 1 end
end

function table:index_of(t, v)
  for i, k in pairs(t) do
    if(k == v) then
      return i
    end
  end
  return -1
end

function table:swap(t, a, b)
  t[a], t[b] = t[b], t[a]
  return t
end


-- function table:find_index(t, f)
--   for i, k in pairs(t) do
--     if(f(t[i])) then
--       return i
--     end
--   end
--   return 0
-- end

function table:find_index(t, f)
  for i = 1, #t do
    if f(t[i]) then 
      return i
    end
  end
  return 0
end

function table:reverse(t)
  local n, m = #t, #t/2
  for i=1, m do
    t[i], t[n-i+1] = t[n-i+1], t[i]
  end
  return t
end


function table:find(t, f)
  for i = 1, #t do
    if f(t[i]) then 
      return t[i]
    end
  end
  return nil
end

function table:filter(t, f)
  local ls = {}
  for i, k in pairs(t) do
    if f(k) then
      table.insert(ls, k)
    end
  end
  return ls
end

function clamp_in_list(value, ls)
  if ls == nil then return nil
  elseif #ls == 0 then return nil
  elseif #ls == 1 then return ls[1]
  else
    local last = ls[1].index
    for i = 2, #ls do
      if value < ls[i].index then
        return last
      end
      last = ls[i].index
    end
    return last
  end
end

function midi_note(v)
  local notes = {"C-", "C#", "D-", "D#", "E-","F-", "F#","G-", "G#","A-", "A#","B-"}
  local n = v % 12
  local o = math.floor(v / 12)
  return notes[n + 1] .. o
end


function table:map(ts, f)
  local ls = {}
  local i = 1
  for k, x in pairs(ts) do
    table.insert(ls, f(x, i))
    i = i + 1
  end
  return ls
end

function table:scroll(t, offset)
  if offset == 1 then
    table.insert(t, #t, t[0])
    table.remove(t, 1)
  else
    table.insert(t, 1, t[0])
    table.remove(t, #t)
  end

  return t
end

function text_match(source, query)
  local s = string.lower(source)
  local q = string.lower(query)
  return s:find(q) ~= nil
end

function string:split(s, separator)
  local a = {}
  for str in string.gmatch(s, "([^" .. separator .. "]+)") do
    table.insert(a, str)
  end
  if #a == 0 then
    return {s}
  else
    return a
  end
end



function string:join(strings, separator)
  local a = ""
  for i, s in pairs(strings) do
    if i == 1 then
      a = s
    else
      a = a..separator..s
    end
  end
  return a
end

function wrap(v, x, l)
  local r = ((v + x - 1) % l) + 1
  if(x == -1 and v == 1) then
    r = l
  end
  return r
end

function capitalize_all(text)
  local words = string:split(text, " ")
  local capped = table:map(words, function(word) 
    local l = word:gsub("^%l", string.upper)
    return l 
  end 
  )
  return string:join(capped, " ")
end
function  section_end(s, sec)
  for i = sec + 1, #s.sequencer.pattern_sequence do
    if s.sequencer:sequence_is_start_of_section(i) then
      return i - 1
    end
  end
  return sec
end
function find_section_index(start)
  local i = start
  while i > 0 do
    if renoise.song().sequencer:sequence_is_start_of_section(i) then
      return i
    end            
    i = i - 1
  end
  return 1
end

function get_sections(s)
  local ss = {}
  for i = 1, #s.sequencer.pattern_sequence do
    if s.sequencer:sequence_is_start_of_section(i) then
      table.insert(ss, {index = i, name = s.sequencer:sequence_section_name(i)})
    end
  end

  if #ss == 0 then 
    return {{index = 1, name = "no sections"}}
  else 
    return ss
  end
end

function selection_in_matrix()
  local song = renoise.song()
  local seq = song.sequencer
  local selection_range = function(start_track, start_line, end_track, end_line)
    return {
      start_track = start_track,
      end_track = end_track,
      start_line = start_line,
      end_line = end_line
    }
  end
  local get_start = function()
    for s = 1, #seq.pattern_sequence do
      for t = 1, #song.tracks do
        if seq:track_sequence_slot_is_selected(t, s) then
          return selection_range(t,s,t,s)
        end
      end
    end
  end
  local get_end = function(selection)
    for s = #seq.pattern_sequence, 1, -1 do
      for t = #song.tracks, selection.start_track, -1 do
        if seq:track_sequence_slot_is_selected(t, s) then
          return selection_range(selection.start_track, selection.start_line, t, s)
        end
      end
    end
    return selection
  end
  
  return get_end(get_start())
end



function as_instrument_index(i)
  return string.upper(string.format("%02x", i - 1))
end

function find_sample_index(instrument_index, sample_index)
  local s = renoise.song()
  local overall_index = 1
  for i = 1, #s.instruments do
    local instrument = s:instrument(i)
    if i == instrument_index then
      return overall_index + sample_index - 1
    else
      for si = 1, #instrument.samples do
        overall_index = overall_index + 1
      end
    end
  end
  return ls
end

function get_all_samples()
  local s = renoise.song()
  local ls = {}
  local overall_index = 1
  for i = 1, #s.instruments do
    local instrument = s:instrument(i)
    for si = 1, #instrument.samples do
      local sample = instrument:sample(si)
      table.insert(ls, {
        sample_index = si, 
        index = overall_index, 
        instrument = i, 
        name = sample.name,
        note = midi_note(sample.sample_mapping.base_note), 
      })
      overall_index = overall_index + 1
    end
  end
  return ls
end

function  as_dsp_device_index(device_data)
  return (device_data.active and "#" or "-") --  .. device_data.device_index
end

function find_track_device_index(track_index, device_index)
  local s = renoise.song()
  local overall_index = 1
  for i = 1, #s.tracks do
    for di = 1, #s:track(i).devices do
      if i == track_index and di == device_index then
        return overall_index
      end
      overall_index = overall_index + 1
    end
  end
  return 1
end

function select_dsp_device(s, device)
  if device.type == "track device" then
    show_frame("dsp")
    s.selected_track_index = device.track_index
    s.selected_track_device_index = device.device_index
  elseif device.type == "sample device" then
    show_frame("effects")
    s.selected_instrument_index = device.instrument_index
    s.selected_sample_device_chain_index = device.chain_index
    s.selected_sample_device_index = device.device_index
  end
end

function swap_devices_at(container, index, dir)
  if dir < 0 then
    if index > 2 then
      container:swap_devices_at(index, index - 1)
    end
  else
    if index < #container.devices then
      container:swap_devices_at(index, index + 1)
    end
  end
end

function find_sample_device_index(instrument_index, chain_index, device_index)
  local s = renoise.song()
  local overall_index = 1
  for i = 1, #s.tracks do
    for di = 1, #s:track(i).devices do
      overall_index = overall_index + 1
    end
  end

  for i = 1, #s.instruments do
    local instrument = s:instrument(i)
    for ci = 1, #instrument.sample_device_chains do
      local chain = instrument.sample_device_chains[ci]
      for di = 1, #chain.devices do
        if i == instrument_index and ci == chain_index and device_index == di then
          return overall_index
        end
        overall_index = overall_index + 1
      end
    end
  end

  return 1
end

function get_all_dsp_devices()
  local s = renoise.song()
  local ls = {}
  local overall_index = 1
  for i = 1, #s.tracks do
    local track = s:track(i)
    for di = 1, #track.devices do
      local device = track.devices[di]
      table.insert(ls, {
        device_index = di, 
        index = overall_index, 
        track_index = i, 
        name = device.short_name,
        active = device.is_active,
        location_name = track.name,
        type = "track device",
      })
      overall_index = overall_index + 1
    end
  end

  for i = 1, #s.instruments do
    local instrument = s:instrument(i)
    for ci = 1, #instrument.sample_device_chains do
      local chain = instrument.sample_device_chains[ci]
      for di = 1, #chain.devices do
        local device = chain.devices[di]
        table.insert(ls, {
          device_index = di, 
          index = overall_index, 
          chain_index = ci, 
          instrument_index = i, 
          name = device.short_name,
          active = device.is_active,
          type = "sample device",
          location_name = instrument.name .. " / ".. chain.name,
        })
        overall_index = overall_index + 1
      end
    end
  end
  return ls
end


function get_track_dsp_parameters(track_index, device_index)
  local s = renoise.song()
  local ls = {}
  for i = 1, #s.tracks[track_index].devices[device_index].parameters do
    local parameter = s.tracks[track_index].devices[device_index].parameters[i]
    table.insert(ls, {
      index = i,
      name = parameter.name,
      min = parameter.value_min,
      max = parameter.value_max,
      default = parameter.value_default,
      value = parameter.value,
      value_string = parameter.value_string,
      parameter = parameter,
    })
  end
  return ls
end

function log(t)
  renoise.app():show_status("PAL : " .. t)
end

function get_sample_dsp_parameters(instrument_index, chain_index, device_index)
  local s = renoise.song()
  local ls = {}
  local d = s.instruments[instrument_index].sample_device_chains[chain_index].devices[device_index]
  for i = 1, #d.parameters do
    local parameter = d.parameters[i]
    table.insert(ls, {
      index = i,
      name = parameter.name,
      min = parameter.value_min,
      max = parameter.value_max,
      default = parameter.value_default,
      value = parameter.value,
      value_string = parameter.value_string,
      parameter = parameter,
    })
  end
  return ls
end

function  get_instrument_name(i)
  return i.name .. (i.plugin_properties.plugin_device and " ("..i.plugin_properties.plugin_device.short_name..") " or "")
end
function match(x, ls)
  for key, value in pairs(ls) do
    if x == key then return value end
  end
  return ls["_"]
end


function ifelse(c, r, e)
  if c then return r else return e end
end

function tryload(l)
  local success, lib = pcall(require, l)
  if(success) then return lib
  else return nil end
end


function include(dir, module)
  local path = "../" .. dir .. "/" .. module .. ".lua"
  local file = io.open(path, "rb")
  if(file ~= nil) then
    package.path = package.path .. ";".. "../" .. dir .. "/?.lua"
    tryload(module)
    package.path = package.path .. ";./?.lua"
    -- return m
  end
  return nil

  -- else
  --   local message = path .. " not found!\n make sure you have " .. dir .. " installed!"
  --   renoise.app():show_prompt("missing dependecy!", message, {"OK"})
  -- end
end


function show_frame(t)
  local a = renoise.app()
  local w = a.window
  local show_middle = function(m) w.active_middle_frame = m end
  local f = match(t, {
    matrix = function() 
      show_middle(renoise.ApplicationWindow.MIDDLE_FRAME_PATTERN_EDITOR)
      w.pattern_matrix_is_visible = true
    end,
    pattern = function() 
      show_middle(renoise.ApplicationWindow.MIDDLE_FRAME_PATTERN_EDITOR)
    end,
    sample = function() 
      show_middle(renoise.ApplicationWindow.MIDDLE_FRAME_INSTRUMENT_SAMPLE_EDITOR)
    end,
    phrase = function() 
      show_middle(renoise.ApplicationWindow.MIDDLE_FRAME_INSTRUMENT_PHRASE_EDITOR)
    end,
    effects = function() 
      show_middle(renoise.ApplicationWindow.MIDDLE_FRAME_INSTRUMENT_SAMPLE_EFFECTS)
    end,
    modulation = function() 
      show_middle(renoise.ApplicationWindow.MIDDLE_FRAME_INSTRUMENT_SAMPLE_MODULATION)
    end,
    keyzones = function() 
      show_middle(renoise.ApplicationWindow.MIDDLE_FRAME_INSTRUMENT_SAMPLE_KEYZONES)
    end,
    mixer = function() 
      show_middle(renoise.ApplicationWindow.MIDDLE_FRAME_MIXER)
      w.lower_frame_is_visible = true
    end,
    dsp = function() 
      if w.active_middle_frame ~= renoise.ApplicationWindow.MIDDLE_FRAME_PATTERN_EDITOR or w.active_middle_frame ~= renoise.ApplicationWindow.MIDDLE_FRAME_MIXER then
        show_middle(renoise.ApplicationWindow.MIDDLE_FRAME_PATTERN_EDITOR)
      end
      -- show_middle(renoise.ApplicationWindow.MIDDLE_FRAME_MIXER)
      -- show_middle(renoise.ApplicationWindow.MIDDLE_FRAME_MIXER)
      w.lower_frame_is_visible = true
      w.active_lower_frame = renoise.ApplicationWindow.LOWER_FRAME_TRACK_DSPS
    end,
    midi = function() 
      show_middle(renoise.ApplicationWindow.MIDDLE_FRAME_INSTRUMENT_MIDI_EDITOR)
    end,
    plugin = function() 
      show_middle(renoise.ApplicationWindow.MIDDLE_FRAME_INSTRUMENT_PLUGIN_EDITOR)
    end
  })()
end

function middle_frame_is(frames)
  for i = 1, #frames do
    if renoise.app().window.active_middle_frame == frames[i] then return true end
  end
  return false
end

function tracks_visible()
  return middle_frame_is({
    renoise.ApplicationWindow.MIDDLE_FRAME_MIXER,
    renoise.ApplicationWindow.MIDDLE_FRAME_PATTERN_EDITOR
  })
end

function sampler_visible()
  return middle_frame_is({
    renoise.ApplicationWindow.MIDDLE_FRAME_INSTRUMENT_PHRASE_EDITOR,
    renoise.ApplicationWindow.MIDDLE_FRAME_INSTRUMENT_SAMPLE_EDITOR,
    renoise.ApplicationWindow.MIDDLE_FRAME_INSTRUMENT_SAMPLE_KEYZONES,
    renoise.ApplicationWindow.MIDDLE_FRAME_INSTRUMENT_SAMPLE_EFFECTS,
    renoise.ApplicationWindow.MIDDLE_FRAME_INSTRUMENT_SAMPLE_MODULATION,
  })
end

function find_sequence_index_with_pattern(p)
  local s = renoise.song()

  for i = 1, #s.sequencer.pattern_sequence do
    if p == s.sequencer.pattern_sequence[i] then 
      return i
    end
  end
  return s.selected_sequence_index
end